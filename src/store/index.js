import Vue from 'vue'
import Vuex from 'vuex'
import data from '@/components/Allgoods/data.js'

function setItem(name, item) {
  window.sessionStorage.setItem(name, JSON.stringify(item))
}
function getItem(name) {
  return JSON.parse(window.sessionStorage.getItem(name) || '[]')
}

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    // 二级分类的数据
    RightSrotData: getItem('RightSrotData'),
    // 底部导航栏索引
    Active: getItem('Active'),
    // 商品详情数据
    GoodsData: getItem('GoodsData'),
    // 收集已经收藏的商品ID
    CollectionGoodsId: getItem('CollectionGoodsId'),
    data
  },
  getters: {},
  mutations: {
    SetRightSrotData(state, data) {
      state.RightSrotData = data
      setItem('RightSrotData', state.RightSrotData)
    },
    SetActive(state, index) {
      state.Active = index
      setItem('Active', state.Active)
    },
    SetGoodsData(state, data) {
      state.GoodsData = data
      setItem('GoodsData', state.GoodsData)
    },
    SetCollectionGoodsId(state, id) {
      state.CollectionGoodsId = id
      state.data.forEach((item) => {
        if (item.id === id) {
          item.Abc = true
          console.log(item.Abc)
        }
      })
      setItem('CollectionGoodsId', state.CollectionGoodsId)
    }
  },
  actions: {},
  modules: {}
})
