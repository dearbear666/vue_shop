import Vue from 'vue'
import VueRouter from 'vue-router'
import BottomTab from '@/components/BottomTab/BottomTab.vue'
import Home from '@/components/Home/Home.vue'
import Sort from '@/components/Sort/Sort.vue'
import Sign from '@/components/Sign/Sign.vue'
import Cat from '@/components/Cat/Cat.vue'
import User from '@/components/User/User.vue'
import RightSort from '@/components/Sort/RightSort.vue'
import Login from '@/components/Login/Login.vue'
import Allgoods from '@/components/Allgoods/Allgoods.vue'
import GoodsData from '@/components/GoodsData/GoodsData.vue'
import Integralmall from '@/components/Integralmall/Integralmall.vue'
import IntegralGoods from '@/components/IntegralGoods/IntegralGoods.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/Home/home' },
  { path: '/login', component: Login },
  { path: '/GoodsData', component: GoodsData, name: 'RouterGoodsData', props: true },
  { path: '/IntegralGoods', component: IntegralGoods, name: 'RouterIntegralGoods', props: true },
  {
    path: '/Home',
    component: BottomTab,
    children: [
      { path: '/Home/home', component: Home },
      {
        path: '/Home/sort',
        component: Sort,
        redirect: '/rightsort',
        children: [{ path: '/rightsort', component: RightSort }]
      },
      { path: '/Home/sign', component: Sign },
      { path: '/Home/cat', component: Cat },
      { path: '/Home/user', component: User },
      { path: '/Home/Allgoods', component: Allgoods },
      { path: '/Home/Integralmall', component: Integralmall }
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path === '/Home/user' || to.path === '/Home/Allgoods' || to.path === '/Home/Integralmall') {
    const token = window.sessionStorage.getItem('token')
    if (token) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})

export default router
