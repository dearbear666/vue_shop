export default [
  {
    id: 1,
    icon: 'send-gift-o',
    color: '#9A99FB',
    text: '全部商品'
  },
  {
    id: 2,
    icon: 'gift-o',
    color: '#9A99FB',
    text: '优惠卷'
  },
  {
    id: 3,
    icon: 'shop-o',
    color: '#9A99FB',
    text: '积分商品'
  },
  {
    id: 4,
    icon: 'star-o',
    color: '#9A99FB',
    text: '积分签到'
  }
]
