export default [
  {
    id: 1,
    name: '手机',
    children: [
      {
        id: 11,
        name: '华为'
      },
      {
        id: 22,
        name: '小米'
      },
      {
        id: 33,
        name: '苹果'
      }
    ]
  },
  {
    id: 2,
    name: '电脑',
    children: [
      {
        id: 44,
        name: '华硕'
      },
      {
        id: 55,
        name: '拯救者'
      },
      {
        id: 66,
        name: '外星人'
      }
    ]
  },
  {
    id: 3,
    name: '男装',
    children: [
      {
        id: 44,
        name: '海澜之家'
      },
      {
        id: 55,
        name: '耐克'
      },
      {
        id: 66,
        name: '阿迪达斯'
      }
    ]
  },
  {
    id: 4,
    name: '女装',
    children: [
      {
        id: 44,
        name: '无敌女装'
      },
      {
        id: 55,
        name: '女装无敌'
      },
      {
        id: 66,
        name: '真好'
      }
    ]
  },
  {
    id: 5,
    name: '童装',
    children: [
      {
        id: 44,
        name: '孩子装'
      },
      {
        id: 55,
        name: '小子装'
      },
      {
        id: 66,
        name: '装小子'
      }
    ]
  }
]
